$(function () {

            Fancybox.bind('.js__modal', {
                autoFocus: false,
                trapFocus: false,
                closeButton: 'outside',
            });


            $('.js__btn_close__modal').click(function (e) {
                Fancybox.close();
            });

            $('.js_ancor').click(function(e){
                e.preventDefault();

                $([document.documentElement, document.body]).animate({
                    scrollTop: $($(this).attr('href')).offset().top
                }, 1500);
            })


            // Hide Header on on scroll down
            var didScroll;
            var lastScrollTop = 0;
            var delta = 5;
            var navbarHeight = $('.js_header').outerHeight();

            let topOut = parseInt($('body').css('paddingTop')) + 5;

            $(window).scroll(function (event) {
                didScroll = true;
            });

            setInterval(function () {
                if (didScroll) {
                    hasScrolled();
                    didScroll = false;
                }
            }, 250);

            function hasScrolled() {
                var st = $(this).scrollTop();

                // Make sure they scroll more than delta
                if (Math.abs(lastScrollTop - st) <= delta)
                    return;

                // If they scrolled down and are past the navbar, add class .nav-up.
                // This is necessary so you never see what is "behind" the navbar.
                if (st > lastScrollTop && st > navbarHeight) {
                    // Scroll Down
                    $('.js_header').css('top', "-" + topOut + 'px');
                } else {
                    // Scroll Up
                    if (st + $(window).height() < $(document).height()) {
                        $('.js_header').removeAttr('style');
                    }
                }

                lastScrollTop = st;
            }


            $('.js_testimonials__section__slider').slick({
                infinite: false,
            })

            $('.js_about__section__tab a').click(function (e) {
                e.preventDefault();
                $('.js_about__section__tab a.active').removeClass('active')
                $(this).addClass('active')

                $('.js_about__section__tab__content .about__section__tab__content__item.active').slideUp(400, function () {
                    $(this).removeClass('active')
                })

                $('.js_about__section__tab__content .about__section__tab__content__item').eq($(this).index()).slideDown(400, function () {
                    $(this).addClass('active')
                })

            })
            $.fn.isInViewport = function () {
                let elementTop = $(this).offset().top;
                let elementBottom = elementTop + $(this).outerHeight();

                let viewportTop = $(window).scrollTop();
                let viewportBottom = viewportTop + $(window).height();

                return elementBottom > viewportTop && elementTop < viewportBottom;
            };


            let animateStartCount = false;

            $(window).on('resize scroll', function () {
                if ($('.js_count_animate').isInViewport() && !animateStartCount) {
                    animateStartCount = true;
                    $('.js_count_animate').each(function () {
                        var $this = $(this),
                            countTo = $this.attr('data-number');

                        $({
                            countNum: $this.text()
                        }).animate({
                                countNum: countTo
                            },
                            {
                                duration: 5000,
                                easing: 'linear',
                                step: function () {
                                    $this.text(Math.floor(this.countNum));
                                },
                                complete: function () {
                                    $this.text(this.countNum);
                                }

                            });

                    });
                }

            });




                $('.js_btn_menu,.js_modal__menu__btn_close,.js_modal__menu__overlay,.js__close__menu').click(function (e) {
                    e.preventDefault();
                    $('.js_btn_menu').toggleClass('active')
                    $('.js__modal__menu').toggleClass('active')
                    $('.js_modal__menu__overlay').toggleClass('active')
                });



                $('.js_form').submit(function (event) {
                    if ($(this)[0].checkValidity()) {
                        let formData = $(this).serialize();
                        // let formDataAdmin = $(this).serializeArray();
                        $.ajax({
                            type: "POST",
                            url: "obr.php",
                            data: formData,
                            success: function (msg) {
                                // write_data_admin(formDataAdmin);
                                $.fancybox.open({
                                    src: '#modal_success'
                                })
                            }
                        });
                    }
                });

            });